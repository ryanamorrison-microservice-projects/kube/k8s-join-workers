k8s-join-workers
=========

Joins worker nodes to Kubernetes cluster. If local storage is anticipated, it can also set partition and format a disk so that Rook will ignore it.  This series of roles is more purpose-specific than a general implementation like kube-spray.

Requirements
------------
Assumes certain ansible inventory groups are present in a specific hierarchy:
```
---
firstcp:
  hosts:
    cp1:
      var_host_endpoint_ip: 192.168.1.2
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
restcps:
  hosts:
    cp2:
      var_host_endpoint_ip: 192.168.1.3
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
allcps:
  children:
    firstcp:
    restcps:
  vars:
    var_kube_version: 1.27
    var_kube_install_version: 1.27.1-00
    var_service_subnet_cidr: 10.244.0.0/16
    var_pod_subnet_cidr: 172.20.0.0/16
    var_node_mask_cidr: 24
    var_dns_domain: cluster.local
    var_cluster_name: kubernetes 
    var_storage: rook
    var_cni: cilium
    cilium_install_with_helm: false
    cilium_use_gw_api: false
    cilium_use_kube_proxy: true
    cilium_version: 1.14.6
workers:
  hosts:
    worker1:
      var_host_endpoint_ip: 192.168.1.4
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
    worker2:
      var_host_endpoint_ip: 192.168.1.5
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
  vars:
    var_local_storage: true
    var_localk8s_disk: vdd
allkube:
  children:
    allcps:
    workers:
  vars:
    var_kube_version: 1.27
```
Also see variables and playbook below

Role Variables
--------------
Are broken into two categories based on where they are located.
### default variables
Variable default values listed below.  Sane defaults provided for testing, if using local storage then override these in inventory.     

Whether to use local storage
```
var_local_storage: false
```
The disk device to use as local storage.  This partitions and formats the disk so that Rook will ignore it. 
```
var_localk8s_disk: vdc
```

### inventory variables
each node (ec2 instance, host, QEMU vm, etc.) is assumed to have a ssh key that CI runners will use to connect to the host:
```
ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
```

Dependencies
------------
* k8s-common
* k8s-cps-common
* k8s-init-cluster
* k8s-join-cps (optional, if needed for additional CPs)

Also see example Playbook below.

Example Playbook
----------------
```
---
- hosts: allkube
  become: true
  become_method: sudo
  include_roles:
    - k8s-common

- hosts: allcps
  become: true
  become_method: sudo
  include_roles:
    - k8s-cps-common

- hosts: firstcp
  become: true
  become_method: sudo
  include_roles:
    - k8s-init-cluster

- hosts: restcps
  serial: 1
  become: true
  become_method: sudo
  include_roles:
    - k8s-join-cps

- hosts: workers
  serial: 1
  become: true
  become_method: sudo
  include_roles:
    - k8s-join-workers
```

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
